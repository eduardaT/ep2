## O Jogo
 
O objetivo é coletar o máximo de comida possível, podendo esbarrar nas bordas da tela. Não podendo esbarrar em nenhuma parte do corpo da cobra, que cresce à medida que come, aumentando a dificuldade do jogo.
 
## Requisitos
 
Para jogar, é necessário ter um computador com uma IDE compatível com a linguagem utilizada (Java).
 
## Como Jogar
 
Ao iniciar o programa, para começar a jogar, basta pressionar a tecla para cima, frente ou baixo do teclado. A cobrinha se orienta de acordo com a orientação das setas do teclado. Se a cobrinha encosta no seu próprio corpo, então o jogador perde o jogo e a mensagem "GAMEOVER!" aparece na tela. Para jogar novamente, basta clicar na tecla "enter", o jogo continuara no nível em que o jogador perdeu, a menos que o jogo seja fechado e iniciado novamente. A cada 10 pontos o jogador avança de nível, e a cobrinha fica mais rápida aumentando a dificuldade do jogo.
