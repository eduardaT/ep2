package jogo_cobrinha;

import java.awt.Dimension;

import javax.swing.JFrame; 

public class main {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Jogo Da Cobrinha");
		frame.setContentPane(new Painel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.pack();
		frame.setPreferredSize(new Dimension(Painel.WIDTH, Painel.HEIGHT));
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
